package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;
@RestController
public class UserController {
	
		@Autowired
		UserDAO userDAO;
		
		@PostMapping("registerCustomer")
		public String registerCustomer(@RequestBody User user){
			userDAO.registerCustomer(user);
			
			return "Customer Registered Succesfully";
		}
		
		@GetMapping("loginByEmail/{emailId},{password}")
		public User loginByEmail(@PathVariable("emailId") String emailId ,@PathVariable("password")String password){
			
			return userDAO.loginByEmail(emailId,password);
			}		
	 
	}




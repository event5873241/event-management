package com.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.model.User;

public interface UserRepository extends JpaRepository< User,Integer> {

	@Query("from User u where u.emailId = :emailId and u.password= :password")
	User findByEmailId(@Param("emailId") String emailId ,@Param("password") String password);
	
}

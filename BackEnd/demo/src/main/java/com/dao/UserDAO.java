package com.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.User;

@Service
public class UserDAO {

	@Autowired
	UserRepository userRepository;

	public void registerCustomer(User user) {
		
		userRepository.save(user);
	}

	public User loginByEmail(String emailId, String password) {	
		User user = userRepository.findByEmailId(emailId, password);
		
		if (user.getEmailId().equals(emailId) && user.getPassword().equals(password)) {
			
			return user;
		}
		return null; 
	}


}